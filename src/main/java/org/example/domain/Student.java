package org.example.domain;

import lombok.Data;

/**
 * xxx
 *
 * @author lyl
 * @date 2023/12/30 10:29
 * @desc
 */
@Data
public class Student {

    private Integer id;

    private String name;

    private Integer age;

}
