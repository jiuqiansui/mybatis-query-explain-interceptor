package org.example.mapper;

import org.apache.ibatis.annotations.Param;
import org.example.domain.Student;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * xxx
 *
 * @author lyl
 * @date 2023/12/30 10:27
 * @desc
 */
@Repository
public interface StudentMapper {

    List<Student> getList(@Param("name") String name);

    List<Student> getListByTeacherName(@Param("name") String name);

}
