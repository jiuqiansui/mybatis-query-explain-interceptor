package org.example.web;

import org.example.domain.Student;
import org.example.mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * xxx
 *
 * @author lyl
 * @date 2023/12/30 10:32
 * @desc
 */
@RestController
public class TestController {

    @Autowired
    private StudentMapper studentMapper;

    @GetMapping("/s")
    @ResponseBody
    public List<Student> list(@RequestParam String name) {
        return studentMapper.getList(name);
    }

    @GetMapping("/getByTeacher")
    @ResponseBody
    public List<Student> getByTeacher(@RequestParam String teacherName) {
        return studentMapper.getListByTeacherName(teacherName);
    }

}
